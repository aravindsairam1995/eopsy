// include all necessary packages
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

// function to copy using read/write
void copy_read_write(int sourceFile,int targetFile)
{
    printf("copying using read/write operation!\n");
    // initailise data read variable
	int dataRead;
    // If size of the source file greater than zero
	if(sourceFile >0)
	{
		// Data to read from the source file
		do
		{
            // create a character buffer of size 256
			char buffer[256] = {0};
			// read data in local buffer
			dataRead = read(sourceFile, buffer, sizeof(buffer));
			
			// write the data to target file
			write(targetFile, buffer, dataRead);
            // do unril these is no line to read
		}while(dataRead>0);
		// close the input file when all data is copied
		close(sourceFile);
	}
	// close the output file
	close(targetFile);
    // print the completion msg
	printf("copy complete.\n");
}

// function for copying using mmap
void copy_mmap(int sourceFile,int targetFile)
{
	printf("copying using mmap!\n");
    // initailise file size varibale
	size_t filesize;
	// find the size of file
    filesize = lseek(sourceFile, 0, SEEK_END);
	char *source, *target;
	// map the source file in virtual address space
	source = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, sourceFile, 0);
	// trunccate to fit
	ftruncate(targetFile, filesize);
	// map the target file into process address space
	target = mmap(NULL, filesize, PROT_READ | PROT_WRITE, MAP_SHARED, targetFile, 0);
	
	// copying both the files after mem is set
	memcpy(target, source, filesize);

	// unmap source file
	munmap(source, filesize);
    // unmap source file
	munmap(target, filesize);
	// print the completion msg
	printf("copy complete.\n");

}

//main function
int main(int argc, char* argv[])
{
	// parse command line arguments
	int option = 0;
	// 0 for help 
	// 1 for copying using mmap  
	// 2 for copying with read write
	int usage = 2,u = -1;
    // read the arguments from the command line
	while ((option = getopt(argc, argv,"hm:")) != -1) {
		switch (option) {
                // if h usage becomes 0 and goes for help
			case 'h' : usage = 0;

				   break;
                // if m usage becomes 1 and goes for copying
			case 'm' : usage = 1;

				   break;
                // If no argument goes to default and prints a message
			default: printf("invalid usage of command\n");
				 exit(EXIT_FAILURE);
		}
	}
	// print for invalid usage
	if(argc <= 2)
	{
		printf("invalid usage of command\n");
		exit(EXIT_FAILURE);

	}
	// display help message
	if(usage == 0)
	{
        // prints the help message
		printf("./task6 <output file> -m <input file> **copies content of source file to target file using mmap\n");
		printf("./task6 <source file> <target file> ** copies content of source file to target file using read/write\n");
		printf("copy [-h] prints the help information\n");
		return 0;
	}
	int arg = 0;
	if(usage == 1)
		arg = 3;
	else
		arg = 2;
    // define target file, source file and data read variables
	int targetFile, sourceFile,dataRead;
	// output file opened or created
    // if opening target file failed print a error message
	if((targetFile = open(argv[arg], O_CREAT | O_APPEND | O_RDWR))==-1){
		perror("opening output file");
	}
	// open the source file
    // if opening source file failed print a error message
	sourceFile = open(argv[arg-1], O_RDONLY);
	if(sourceFile == -1)
		perror("opening input file");
	// now call the appropriate function 
	if(usage == 1)
	{
		// call copy using mmap system call
		copy_mmap(sourceFile,targetFile);		
	}
	else
	{
	 // call copy using read/write system calls
		copy_read_write(sourceFile,targetFile);
	}
	return 0;
}




