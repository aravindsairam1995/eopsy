#!/bin/sh 

# Task 1

#Help Command
if [ $1 = "-h" ];then
cat <<END
Do any one of the following:
	./modify.sh -l|-u <dir/file names...>
	./modify.sh sed <pattern> <dir/file names...>
	./modify.sh -h
END
# exit after running the the help text
exit 0
fi

# assign f and s as temporary variable that has any value to replace
# the arguments while doing shift operation
f=n
s=n

# if -l is given by the user
# then file name will be changed from lowercase to uppercase
if [ $1 = "-l" ]; then
# check whether the file exits
# if exits then proceed
	if [ ! -f "$2" ] ; then
		`echo "file $2 does not exists" 1>&2`
	else
# shift arguments one by one and change them to lowercase to uppercase
		while test "x$2" != "x"
		do
			case "$1" in
                	-f|--first) f=y;;
                	-s|--second) s=y;;
                	-w) with_arg "$3"; shift;;
        	esac
		for i in $( echo $2 );
#translate from lowercase to uppercase
			do mv -i $i `echo $i | tr [:upper:] [:lower:]`;
		done
        	shift
		done
	fi

# if -u is given by the user
# then file name will be changed from uppercase to lowercase
elif [ $1 = "-u" ]; then
# check whether the file exits
# if exits then proceed
	if [ ! -f "$2" ] ; then
		`echo "file $2 does not exists" 1>&2`
# shift arguments one by one and change them to uppercase to lowercase
	else
		while test "x$2" != "x"
		do
			case "$1" in
                	-f|--first) f=y;;
                	-s|--second) s=y;;
                	-w) with_arg "$3"; shift;;
        	esac
		for i in $( echo $2 );
#translate from uppercase to lowercase
			do mv -i $i `echo $i | tr [:lower:] [:upper:]`;
		done
        	shift
		done
	fi

# check for sed pattern
# change file name according to the sed pattern 
elif [ $1 = sed ]; then
	k=$(echo "$3" | $1 $2)
	mv -i $3 "$k"
fi



