#include <stdio.h> // standard input/output package
#include <sys/wait.h> // wait package
#include <stdlib.h>//defines four variable types, several macros, and various functions for performing general functions
#include <unistd.h>//header defines miscellaneous symbolic constants and types, and declares miscellaneous functions

#define NUM_CHILD 10

int main(){
    pid_t child_pid;        // declare child process PID
    int i, cnt=0;            // temp count variables
    int w;            // Variable declared for sait
    
    printf("parent[%i]: The parent is starting.\n",getpid()); // printing parent process ID
    for (i = 0; i < NUM_CHILD; ++i) {
        // loop over number of child
        if (!(child_pid = fork())){
            // Child processes are created with fork()
            printf("child [%i]: I'm created.\n",getpid());
            sleep(10); // sleeping for 10 sec for each creation of child
            printf("child [%i]: I've completed execution.\n",getpid()); // complete child process after 10 sec
            exit(0);
        }
        sleep(1); // after completed child execution sleep for 1 sec
    }
    while(1){
        // if there is no child break or print that the child has finished
        w = wait(&child_pid);
        if(w == -1)
            break;
        else{
            printf("child [%i]: I've finished.\n",w);
            cnt++; // increment count to check the number of child process
        }
    }
    if (cnt == NUM_CHILD){
        // check that all child proceeses have finished
    printf("\nAll %d processes were finished.\n", cnt);
    return 0;
    }
}
